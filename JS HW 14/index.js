"use strict";

const switcher = document.getElementById("switcher");
let theme = "yellow";
let themeColor = document.getElementById("themeMode");

let changeThemeMode = function () {
if (localStorage.getItem("themeMode") == "yellow") {
    themeColor.href = "./style1.css";
} else {
    themeColor.href = "./style2.css";
}};

changeThemeMode();

switcher.addEventListener("click", function () {
    if (themeColor.getAttribute("href") == "./style1.css") {
        themeColor.href = "./style2.css";
        theme = "blue";
        window.localStorage.setItem(`themeMode`, theme);
    } else {
        window.localStorage.removeItem(`themeMode`, theme);
        themeColor.href = "./style1.css";
        theme = "yellow";
        window.localStorage.setItem("themeMode", theme);
    }
})

