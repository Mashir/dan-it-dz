"use strict"
// Теоретическое задание:
// Document Object Model (DOM) это объектная модель документа, которая позволяет программам и скриптам получить доступ к содержимому HTML документа и взаимодейстовать с ним.

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function createList(arr, list) {
    list = document.createElement('ul');
    let newArray = arr.map(item => {
        let listItem = document.createElement('li');
        listItem.appendChild(document.createTextNode(item));
        list.appendChild(listItem);

    });
return list;
};

document.getElementById("root").appendChild(createList(arr));