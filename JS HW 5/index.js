// 1) Метод об'єкту - это функції, які знаходяться в об'єкті як його властивості.
// 2) Значення властивостей можуть мати будь-який тип, включаючи інші об'єкти.
// 3) Це означає, що на нього можна послатися за допомогою ідентифікатора. Змінна, що містить посилальний тип, фактично його значення не містить. Вона містить посилання місце у пам'яті, де розміщуються реальні дані.

"use strict";

function createNewUser(newUser) {
    newUser = {
        firstName: prompt("Enter your name!"),
        lastName: prompt("Enter your surname!"),
        getLogin() {
        return newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase()
        }
    };
    console.log(newUser.getLogin());
    return newUser;
};


createNewUser();