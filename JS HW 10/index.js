"use strict";

const tabsTitle = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabsItem');

tabsTitle.forEach(function (item) {
   item.addEventListener('click', function () {
      let currentTab = item;
      let tabId = currentTab.getAttribute('data-tab');
      let activeTab = document.querySelector(tabId);
      
      tabsTitle.forEach(function (item) {
         item.classList.remove('active');
      });
      tabsItems.forEach(function (item) {
         item.classList.remove('active');
      });
      currentTab.classList.add('active');
      activeTab.classList.add('active');
   });
});


