"use strict";

const images = document.querySelectorAll('.image-to-show')
let i = 0;

function slide() {
    if (i === images.length - 1) {
      images[i].classList.remove('active')
      i = 0;
      images[0].classList.add('active');
    } else {
        images[i].classList.remove('active');
      images[i + 1].classList.add('active');
      i++;
    }
  };


let buttons = function() {
    let stopButton = document.querySelectorAll('.stop')
    let resumeButton = document.querySelectorAll('.resume')
    let timer = setInterval(slide, 3000)

stopButton.forEach(item => {
    item.addEventListener("click", stop)
})
function stop() {
    clearInterval(timer)   
}
resumeButton.forEach(item => {
    item.addEventListener('click', resume)
})
function resume() {
    setInterval(slide, 3000);
}
}

buttons();