"use strict"

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(name) {
        this._name = name;
    }

    get name() {
        return this._name;
    }

    set age(age) {
        this._age = age;
    }

    get age() {
        return this._age;
    }

    set salary(salary) {
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, language) {
        super(name, age, salary);
        this._language = language;
    }
    get salary(){
        return this._salary * 3;
    }
}

const person1 = new Programmer ("Billy", 48, 300, "eng");
const person2 = new Programmer ("Vitaliy", 31, 1410, "ukr");
const person3 = new Programmer ("Bohdan", 47, 322, "ukr");

console.log(person1);
console.log(person2);
console.log(person3);


