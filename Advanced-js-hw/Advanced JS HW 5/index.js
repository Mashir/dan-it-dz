const urlUsers = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';

class Card {
    constructor(id, userId, title, body, name, userName, email) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
        this.name = name;
        this.userName = userName;
        this.email = email;
        this.container = document.createElement('div');
        this.container.classList.add('card');
        this.deleteButton = document.createElement('button');

    }
    

    createElements() {
        this.container.innerHTML = `<span>${this.title}</span><p>${this.body}</p><span>${this.name}</span><span><img src="./img/mail.png" alt="">${this.email}</span>`;

        this.deleteButton.innerHTML = 'Delete';
        this.container.append(this.deleteButton);
        this.deleteButton.addEventListener('click', (e) => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                    method: 'DELETE',
                })
                .then(response => {
                    e.target.parentNode.remove();
                })
        })
    }

    render(selector) {
        this.createElements();
        document.querySelector(selector).prepend(this.container);
    }
}

Promise.all([
        fetch(urlUsers)
        .then((res) => res.json()
            .then((data) => data)
        ),
        fetch(urlPosts)
        .then((res) => res.json()
            .then((data) => data)
        )
    ])
    .then((data) => {
        const [userArr, postsArr] = data;
        postsArr.forEach(({
            id,
            userId,
            title,
            body
        }) => {
            const user = userArr.find((item) => item.id === userId);
            new Card(
                id,
                userId,
                title,
                body,
                user.name,
                user.username,
                user.email
            ).render('#root');
        })
    });
