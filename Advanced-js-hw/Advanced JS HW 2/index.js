"use strict"

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  function createList(books, booksList) {
    let div = document.querySelector('#root')
    booksList = document.createElement('ul');
    div.append(booksList);
      
    books.forEach(item => {
      let listItem = document.createElement('li');
      let text = document.createTextNode(item.author + ' ' + item.name + ' ' + item.price);
      listItem.appendChild(text);
      try {
        if (!item.author ) {
          throw new Error("В об'єкті вісутній автор")
        }
        if (!item.name) {
          throw new Error("В об'єкті вісутнє ім'я")
        }
        if (!item.price) {
          throw new Error("В об'єкті вісутня ціна")
        }
        booksList.appendChild(listItem);
      } catch (e) {
        console.log(e);
      }
    });
  };
  createList(books);
  
  