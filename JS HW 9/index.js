"use strict"

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function createList(arr, list) {
    list = document.createElement('ul');
    let newArray = arr.map(item => {
        let listItem = document.createElement('li');
        listItem.appendChild(document.createTextNode(item));
        list.appendChild(listItem);
    });
    return list;
};
document.getElementById("root").appendChild(createList(arr));
