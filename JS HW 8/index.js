"use strict"

let arrParagraph = document.querySelectorAll('p');
for (let element of arrParagraph) {
    element.style.background = '#ff0000'
};

let elementOptionList = document.getElementById('optionsList');
console.log(elementOptionList);


let elementOptionListParent = elementOptionList.parentElement;
console.log(elementOptionListParent);


if (elementOptionList.hasChildNodes()) {
    let elementOptionListNodes = elementOptionList.childNodes;
    for (let element of elementOptionListNodes) {
        let name = element.nodeName;
        let type = element.nodeType;
        console.log(name, type);
        }
    };


let testParagraph = document.getElementById('testParagraph');
testParagraph.textContent = "This is a paragraph";

let mainHeaderElements = document.getElementsByClassName('main-header')
for (let element of mainHeaderElements) {
    let child = element.children;
    for (let element of child) {
        element.classList.add('nav-item');
        console.log(element);
    }
};

let deleteElementClass = document.querySelectorAll('section-title');
for (let element of deleteElementClass) {
    element.classList.remove('section-title');
};
