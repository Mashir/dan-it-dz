"use strict"

// 1) Функции нужны для использовании части кода в разных местах. Упрощает выполнение больших программных структур.
// 2) Аргументы нужно передавать чтобы при вызове функции она сработала как нужно.Эти значения функция принимает в качестве параметров и выполняет то, что задано выполнить с этими значениями.

let num1 = +prompt("Введите первое число!");


while (num1 && isNaN(num1) && typeof num1 !== "number") {
    num1 = +prompt("Введите первое число ещё раз!");
}

let num2 = +prompt("Введите второе число!");

while (num2 && isNaN(num2) && typeof num2 !== "number") {
    num2 = +prompt("Введите второе число ещё раз!");
}

let operator = prompt("Введите операцию!");


function calc(num1, num2, operator) {
    
    switch (operator) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num1 / num2;
    } 
};


alert(calc(num1, num2, operator));