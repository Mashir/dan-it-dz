"use strict"

// 1) Экранирование нужно чтобы символы распознавались как текст, а не часть кода и чтобы интерпретатор неверно не рассматривал что-то в тексте, например кавычки.

function createNewUser() {
    let newUser = {
    _firstName: prompt("Введите имя!"),
    _lastName: prompt("Введите фамилию!"),
    birthday: new Date (prompt("Введите дату в формате ДД.ММ.ГГГГ!")),
    getLogin: function() {
        return this._firstName.slice(0, 1).toUpperCase() + this._lastName.toLowerCase();
        
    },

    getPassword: function() {
        return console.log(this._firstName.slice(0, 1).toUpperCase() + this._lastName.toLowerCase() + this.birthday.getFullYear()); 
    },


    getAge: function () {
        const now = new Date ();
        const diff = now.getFullYear() - newUser.birthday.getFullYear();
        return console.log(`Пользователю ${diff} лет`); 

    },
    
    
};
 return newUser.getPassword(), newUser.getAge(); 
};


createNewUser();





