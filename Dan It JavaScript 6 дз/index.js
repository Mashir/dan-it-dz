"use strict"

// Теоретический вопрос:
// Цикл forEach используется для перебора каждого элемента массива и выполнения указанной функции для каждого из них.



const arr = ['hello', 'world', 23, '23', null];
const dataType = 'string';

let filterBy = (arr, dataType) => 
arr.filter((arr) => (typeof arr !== dataType));

console.log(filterBy(arr, dataType));